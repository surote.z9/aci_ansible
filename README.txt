# play with command
ansible-playbook {{name.yaml}} -i ../inventory --ask-vault-password

# procedure run
# Create Vlan pool
# Create Physical Domain
# Play MAP_AAEP_DOMAIN.yaml
# Create Interface Policy Group
# Play CREATE_ISLPORT_AT_LEAFPROFILE.yaml
# Create Tenant/AP/EPG/BD
# Play MAP_PHYS_DOMAIN_EPG.yaml
# Play MAP_STATICPORT_EPG.yaml
